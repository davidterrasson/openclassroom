import pandas as pd
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.collections import LineCollection
import seaborn as sns
from sklearn import preprocessing, cluster, metrics, decomposition
from sklearn.cluster import KMeans
from scipy.cluster.hierarchy import dendrogram, linkage, fcluster
sns.set()
from IPython.display import Markdown as md
from sklearn.preprocessing import StandardScaler
import sys
from scipy.cluster import hierarchy
from scipy.stats import f
from scipy import stats
if not sys.warnoptions:
    import warnings
    warnings.simplefilter("ignore")


def infosurdf(df):
    d1 = pd.DataFrame(df.dtypes).T.rename(index={0: "type de la colonne"})
    d2 = pd.DataFrame(df.isnull().sum()).T.rename(index={0: "nombre de valeurs NaN"})
    d3 = pd.DataFrame(df.isnull().sum()/df.shape[0]*100).T.rename(index={0: "valeurs nulles en %"})
    d4 = pd.DataFrame(df.nunique()).T.rename(index={0: "nombre de valeurs uniques"})
    d5 = pd.DataFrame(df.count()).T.rename(index={0: "nombre de valeurs totales"})
    return pd.concat([d1, d2, d3, d4, d5])


def display_circles(pcs, n_comp, pca, axis_ranks, labels=None, label_rotation=0, lims=None):
    count=10000
    for d1, d2 in axis_ranks:

        if d2 < n_comp:

            # initialisation de la figure
            fig, ax = plt.subplots(figsize=(10,9))

            # détermination des limites du graphique
            if lims is not None:
                xmin, xmax, ymin, ymax = lims
            elif pcs.shape[1] < 30:
                xmin, xmax, ymin, ymax = -1, 1, -1, 1
            else:
                xmin, xmax, ymin, ymax = min(pcs[d1, :]), max(pcs[d1, :]), min(pcs[d2, :]), max(pcs[d2, :])

            # affichage des flèches
            # s'il y a plus de 30 flèches, on n'affiche pas le triangle à leur extrémité
            if pcs.shape[1] < 30:
                plt.quiver(np.zeros(pcs.shape[1]), np.zeros(pcs.shape[1]),
                   pcs[d1, :], pcs[d2, :], 
                   angles='xy', scale_units='xy', scale=1, color="#fc5b6b",headwidth=10)
            else:
                lines = [[[0, 0],[x, y]] for x, y in pcs[[d1, d2]].T]
                ax.add_collection(LineCollection(lines, axes=ax, alpha=.1, color='black'))
            
            # affichage des noms des variables  
            if labels is not None:  
                for i,(x, y) in enumerate(pcs[[d1, d2]].T):
                    if x >= xmin and x <= xmax and y >= ymin and y <= ymax:
                        plt.text(x, y, labels[i], fontsize='14', ha='center', va='center', rotation=label_rotation,fontweight='bold', color="#ffbf00")
            
            # affichage du cercle
            circle = plt.Circle((0,0), 1, facecolor='none', edgecolor='white',linewidth=3)
            plt.gca().add_artist(circle)

            # définition des limites du graphique
            plt.xlim(xmin, xmax)
            plt.ylim(ymin, ymax)
            plt.xticks(fontsize=14)
            plt.yticks(fontsize=14)

            # affichage des lignes horizontales et verticales
            plt.plot([-1, 1], [0, 0], color='white', ls='--',linewidth=3)
            plt.plot([0, 0], [-1, 1], color='white', ls='--',linewidth=3)

            # nom des axes, avec le pourcentage d'inertie expliqué
            plt.xlabel('F{} ({}%)'.format(d1+1, round(100*pca.explained_variance_ratio_[d1],1)),fontsize=14)
            plt.ylabel('F{} ({}%)'.format(d2+1, round(100*pca.explained_variance_ratio_[d2],1)),fontsize=14)
#             plt.savefig( "test.png", transparent=True)
            plt.savefig(str(count) + ".png", transparent=True,bbox_inches='tight')
            plt.title("Cercle des corrélations (F{} et F{})".format(d1+1, d2+1), fontsize=16, fontweight='bold')
            plt.show(block=False)
            count += 1
        
def display_factorial_planes(   X_projected, 
                                x_y, 
                                pca=None, 
                                labels = None,
                                clusters=None, 
                                alpha=1,
                                figsize=[10,8], 
                                marker="." ):
    """
    Affiche la projection des individus

    Positional arguments : 
    -------------------------------------
    X_projected : np.array, pd.DataFrame, list of list : la matrice des points projetés
    x_y : list ou tuple : le couple x,y des plans à afficher, exemple [0,1] pour F1, F2

    Optional arguments : 
    -------------------------------------
    pca : sklearn.decomposition.PCA : un objet PCA qui a été fit, cela nous permettra d'afficher la variance de chaque composante, default = None
    labels : list ou tuple : les labels des individus à projeter, default = None
    clusters : list ou tuple : la liste des clusters auquel appartient chaque individu, default = None
    alpha : float in [0,1] : paramètre de transparence, 0=100% transparent, 1=0% transparent, default = 1
    figsize : list ou tuple : couple width, height qui définit la taille de la figure en inches, default = [10,8] 
    marker : str : le type de marker utilisé pour représenter les individus, points croix etc etc, default = "."
    """

    # Transforme X_projected en np.array
    X_ = np.array(X_projected)

    # On définit la forme de la figure si elle n'a pas été donnée
    if not figsize: 
        figsize = (7,6)

    # On gère les labels
    if  labels is None : 
        labels = []
    try : 
        len(labels)
    except Exception as e : 
        raise e

    # On vérifie la variable axis 
    if not len(x_y) ==2 : 
        raise AttributeError("2 axes sont demandées")   
    if max(x_y )>= X_.shape[1] : 
        raise AttributeError("la variable axis n'est pas bonne")   

    # on définit x et y 
    x, y = x_y

    # Initialisation de la figure       
    fig, ax = plt.subplots(1, 1, figsize=figsize)

    # On vérifie s'il y a des clusters ou non
    c = None if clusters is None else clusters
 
    # Les points    
    plt.scatter(   X_[:, x], X_[:, y], alpha=alpha, 
                        c=c, cmap="Set1", marker=marker)
    sns.scatterplot(data=None, x=X_[:, x], y=X_[:, y], hue=c)

    # Si la variable pca a été fournie, on peut calculer le % de variance de chaque axe 
    if pca : 
        v1 = str(round(100*pca.explained_variance_ratio_[x]))  + " %"
        v2 = str(round(100*pca.explained_variance_ratio_[y]))  + " %"
    else : 
        v1=v2= ''

    # Nom des axes, avec le pourcentage d'inertie expliqué
    ax.set_xlabel(f'F{x+1} {v1}')
    ax.set_ylabel(f'F{y+1} {v2}')

    # Valeur x max et y max
    x_max = np.abs(X_[:, x]).max() *1.1
    y_max = np.abs(X_[:, y]).max() *1.1

    # On borne x et y 
    ax.set_xlim(left=-x_max, right=x_max)
    ax.set_ylim(bottom= -y_max, top=y_max)

    # Affichage des lignes horizontales et verticales
    plt.plot([-x_max, x_max], [0, 0], color='grey', alpha=0.8)
    plt.plot([0,0], [-y_max, y_max], color='grey', alpha=0.8)

    # Affichage des labels des points
    if len(labels) : 
        for i,(_x,_y) in enumerate(X_[:,[x,y]]):
            plt.text(_x, _y+0.05, labels[i], fontsize='14', ha='center',va='center') 

    # Calculate the centroids
    centroids = [np.mean(X_projected[clusters == i], axis=0) for i in np.unique(clusters)]

    # Extract the x and y coordinates of the centroids
    centroid_x = [c[x] for c in centroids]
    centroid_y = [c[y] for c in centroids]

    # Plot the centroids on top of the scatter plot
    plt.scatter(centroid_x, centroid_y, marker='^', c='red', s=220)
        
    
    # plt.savefig(str(count) + ".png", transparent=True)
    # Titre et display
    plt.title(f"Projection des individus (sur F{x+1} et F{y+1})")
    plt.show()
    # count += 1


def display_scree_plot(pca):
    count=1000
    scree = pca.explained_variance_ratio_*100
    kaiser = 100 / 6
    plt.figure(figsize=(15, 10))
    plt.bar(np.arange(len(scree))+1, scree)
    plt.plot(np.arange(len(scree))+1, scree.cumsum(), c="red", marker='o')
    plt.axhline(y=kaiser, color='red', linestyle='--')
    plt.xticks(np.arange(len(scree))+1,size=22)
    plt.yticks(size=22)
    plt.xlabel("Rang de l'axe d'inertie",size=22)
    plt.ylabel("%  d'inertie",size=22)        
    plt.savefig(str(count) + ".png",bbox_inches='tight', transparent=True,)
    plt.title("Eboulis des valeurs propres", fontsize=16, fontweight='bold')
#     plt.show(block=False)
    scree_cum = (pca.explained_variance_ratio_*100).cumsum()
    scree_cum = pd.DataFrame(data=scree_cum, index=np.arange(len(scree))+1, columns=['Cumul de variance expliquée']).T
    print(scree_cum)
    count += 1
    

def matrice_correlation(df):
    plt.figure(figsize=(15,15))
    mask = np.triu(np.ones_like(df.corr(), dtype=bool))
    ax = sns.heatmap(df.corr(), mask=mask, center=0, cmap='RdBu_r', 
                linewidths=1, annot=True, fmt=".2f", vmin=-1, vmax=1, annot_kws={"size": 20}, 
                xticklabels=False, yticklabels=False, cbar_kws={"shrink": 0.5})
    cbar = ax.collections[0].colorbar
    cbar.ax.tick_params(labelsize=20)
    ax.grid(False)
    plt.savefig('Carte des corrélations.png', transparent=True, bbox_inches='tight')
    plt.show()

def matrice_corr(df):
    plt.figure(figsize=(15,15))
    mask = np.triu(np.ones_like(df.corr(), dtype=bool))
    sns.heatmap(df.corr(), mask=mask, center=0, cmap='RdBu', 
                linewidths=1, annot=True, fmt=".2f", vmin=-1, vmax=1,annot_kws={"size": 20})
    plt.xticks(fontsize=20,rotation=85)
    plt.yticks(fontsize=20,rotation=5)
    plt.savefig('Carte des corrélations.png', transparent=True, bbox_inches='tight')
    plt.title('Carte des corrélations', fontsize = 22, fontweight='bold')
    plt.show()
    
def plot_dendrogram(Z, names, orient):
    count=1020
    plt.figure(figsize=(10, 25))  
    plt.title('Classification Ascendante Hiérarchique \n', fontsize=16, fontweight='bold')
    plt.xlabel('Distance')
    plt.grid(color='white')
    plt.axvline(linestyle='--', x=10,color ="red") 
    dendrogram(Z, labels = names, orientation = orient, leaf_font_size=10, color_threshold=10)
    plt.savefig(str(count) + ".png", transparent=True)
    count += 1
    
def display_factorial_planesV2(X_projected, n_comp, pca, axis_ranks, labels=None, alpha=1, illustrative_var=None):
    for d1,d2 in axis_ranks:
        if d2 < n_comp:

            # initialisation de la figure       
            fig = plt.figure(figsize=(7,6))
        
            # affichage des points
            if illustrative_var is None:
                plt.scatter(X_projected[:, d1], X_projected[:, d2], alpha=alpha)
            else:
                illustrative_var = np.array(illustrative_var)
                for value in np.unique(illustrative_var):
                    selected = np.where(illustrative_var == value)
                    plt.scatter(X_projected[selected, d1], X_projected[selected, d2], alpha=alpha, label=value)
                plt.legend()

            # affichage des labels des points
            if labels is not None:
                for i,(x,y) in enumerate(X_projected[:,[d1,d2]]):
                    plt.text(x, y, labels[i],
                              fontsize='14', ha='center',va='center') 
                
            # détermination des limites du graphique
            boundary = np.max(np.abs(X_projected[:, [d1,d2]])) * 1.1
            plt.xlim([-boundary,boundary])
            plt.ylim([-boundary,boundary])
        
            # affichage des lignes horizontales et verticales
            plt.plot([-100, 100], [0, 0], color='grey', ls='--')
            plt.plot([0, 0], [-100, 100], color='grey', ls='--')

            # nom des axes, avec le pourcentage d'inertie expliqué
            plt.xlabel('F{} ({}%)'.format(d1+1, round(100*pca.explained_variance_ratio_[d1],1)))
            plt.ylabel('F{} ({}%)'.format(d2+1, round(100*pca.explained_variance_ratio_[d2],1)))
    	    #plt.savefig(str(count) + ".png", transparent=True)
            plt.title("Projection des individus (sur F{} et F{})".format(d1+1, d2+1))
            plt.show(block=False)    
            #count += 1
    plt.show()

